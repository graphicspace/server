FROM node:8.16.0-alpine
MAINTAINER Arseniy Popov <parsenicum@gmail.com>

WORKDIR /

COPY package*.json ./

RUN npm install

COPY . ./

EXPOSE 3000
CMD [ "node", "index.js" ]
