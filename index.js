"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const TipsApi_1 = require("./api/TipsApi");
const app = express();
const tipsApi = new TipsApi_1.default();
app.use(bodyParser.json());
app.use(cors());
app.post('/', ({ body }, res) => {
    res.json(body.query ? tipsApi.push(body.query, body.userId) : { error: "no query in body" });
});
app.get('/top', async (_, res) => {
    try {
        const result = await tipsApi.top();
        res.json({
            result
        });
    }
    catch (e) {
        res.json({ error: "server error" });
    }
});
app.get('/', ({ query }, res) => {
    if (query.query) {
        tipsApi.get(query.query, query.userId)
            .then(result => res.json({ result }))
            .catch(err => res.json({
            error: "server error"
        }));
    }
    else {
        res.json({
            error: "no query in query"
        });
    }
});
app.all('*', (_, res) => res.send('welcome to tips server'));
app.listen(3000, () => {
    console.log('server started on port 3000');
});
